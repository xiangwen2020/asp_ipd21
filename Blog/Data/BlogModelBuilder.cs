using Blog.Models;
using Microsoft.EntityFrameworkCore;
namespace Blog.Data
{
    public static class  BlogModelBuilder
    {
        public static ModelBuilder Seed(this ModelBuilder modelBuilder){
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    UserName = "Lisa",
                    Password ="Lisa123"
                },
                new User
                {
                    Id = 1,
                    UserName = "Carry",
                    Password ="Carry123"
                },
                new User
                {
                    Id = 1,
                    UserName = "Lenny",
                    Password ="Lenny123"
                }
                
               
               
            );
            return modelBuilder;
        }
    }
}