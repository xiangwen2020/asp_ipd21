using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Blog.Data;
using Blog.Models;
using Blog.Pages;



namespace Blog.Pages
{
    public class ArticleAddModel: PageModel
    {
        private readonly BlogContext db;  
        public ArticleAddModel(BlogContext db) => this.db = db;
        
        public Article Article { get; set;}

        [BindProperty, Required,StringLength(100,MinimumLength = 1,ErrorMessage ="Please add your Article Title,1-100 chars")]
        public string Title { get; set; }
        [BindProperty, StringLength(10000,MinimumLength = 1,ErrorMessage ="Please add your Article Body,the range is 1-10000" ), Required]
        public string Body { get; set; } 

       
   public IActionResult OnPost(){
      Article Article = new Article();
      Article.Title = Title;
      Article.Body = Body;
      Article.Timestamp = DateTime.Now;
       
      if(ModelState.IsValid){
          db.Add(Article);
          db.SaveChanges();
          return RedirectToPage("ArticleAddSuccess");
      }
          return Page();
   }



    }
}