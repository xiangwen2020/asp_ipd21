namespace Bakery.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        //[Column("ImageFileName)] -done using configurations
        public string ImageName { get; set; }
    }
}