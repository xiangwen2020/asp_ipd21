using System;
using System.Threading.Tasks;
using MyFriends.Data;
using MyFriends.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace MyFriends.Pages
{
    public class DeleteModel : PageModel
    {
        private MyFriendsContext db;
        public DeleteModel(MyFriendsContext db) => this.db = db;
        [BindProperty(SupportsGet =true)]
        public int Id { get; set; }
        public Friend Friend { get; set;}
        public async Task OnGetAsync() =>  Friend = await db.Friends.FindAsync(Id);
        
            public IActionResult OnPost(){
            Friend = db.Friends.Find(Id);
            if(ModelState.IsValid){
                db.Remove(Friend);
                db.SaveChanges();
                return RedirectToPage("DeleteSuccess");
            }
            return Page();
        }
        
    }
}