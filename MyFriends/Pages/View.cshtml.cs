using System;
using System.Threading.Tasks;
using MyFriends.Data;
using MyFriends.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace MyFriends.Pages
{
    public class ViewModel : PageModel
    {
        private MyFriendsContext db;
        public ViewModel(MyFriendsContext db) => this.db = db;
        [BindProperty(SupportsGet =true)]
        public int Id { get; set; }
        public Friend Friend { get; set;}
        public async Task OnGetAsync() =>  Friend = await db.Friends.FindAsync(Id);
        
          [BindProperty, Required,StringLength(100,MinimumLength = 1,ErrorMessage ="Please add your friend name,1-100 chars")]
        public string Name { get; set; }
        [BindProperty, Range(0,100,ErrorMessage ="Please add your friend age,the range is 0-100" ), Required]
        public int Age { get; set; } 
            public IActionResult OnPost(){
            Friend = db.Friends.Find(Id);
            if(ModelState.IsValid){
                
            Friend.Age = Age;
            Friend.Name = Name;
                db.SaveChanges();
                return RedirectToPage("Index");
            }
            return Page();
        }
        
    }
}