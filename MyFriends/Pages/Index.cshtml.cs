﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyFriends.Data;
using MyFriends.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
namespace MyFriends.Pages
{
    public class IndexModel : PageModel
    {
        private readonly MyFriendsContext db;  
        public IndexModel(MyFriendsContext db) => this.db = db;
        public List<Friend> Friends { get; set; } = new List<Friend>();  
        
        public async Task OnGetAsync()
        {
            Friends = await db.Friends.ToListAsync();
        
        }
        // public void OnGet()
        // {
        //     Friends = db.Friends.ToList();
        
        // }
    }
}