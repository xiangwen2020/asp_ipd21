using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MyFriends.Data;
using MyFriends.Models;
using MyFriends.Pages;



namespace MyFriends.Pages
{
    public class AddModel : PageModel
    {
        private readonly MyFriendsContext db;  
        public AddModel(MyFriendsContext db) => this.db = db;
        
        public Friend Friend { get; set;}

        [BindProperty, Required,StringLength(100,MinimumLength = 1,ErrorMessage ="Please add your friend name,1-100 chars")]
        public string Name { get; set; }
        [BindProperty, Range(0,100,ErrorMessage ="Please add your friend age,the range is 0-100" ), Required]
        public int Age { get; set; } 

       
   public IActionResult OnPost(){
      Friend friend = new Friend();
      friend.Age = Age;
      friend.Name = Name;
      if(ModelState.IsValid){
          db.Add(friend);
          db.SaveChanges();
          return RedirectToPage("AddedSuccess");
      }
          return Page();
   }



    }
}