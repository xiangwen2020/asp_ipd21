using MyFriends.Models;
using Microsoft.EntityFrameworkCore;
namespace MyFriends.Data
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder Seed(this ModelBuilder modelBuilder){
            modelBuilder.Entity<Friend>().HasData(
                new Friend
                {
                    Id = 1,
                    Name = "Carry",
                    Age = 22
                },
                new Friend
                {
                     Id = 2,
                    Name = "Marry",
                    Age = 23
                },
                new Friend
                {
                    Id = 3,
                    Name = "Sarry",
                    Age = 45

                   
                },
                new Friend
                { Id = 4,
                    Name = "Tarry",
                    Age = 65
                    
                   
                },
                new Friend
                {
                    Id = 5,
                   Name = "Linda",
                    Age = 34
                },
                new Friend
                {
                    Id = 6,
                   Name = "Lisa",
                    Age = 42
                }
            );
            return modelBuilder;
        }
    }
}