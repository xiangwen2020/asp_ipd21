﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyFriends.Data.Migrations
{
    public partial class CreateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Friends",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Friends", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Friends",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { 1, 22, "Carry" });

            migrationBuilder.InsertData(
                table: "Friends",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { 2, 23, "Marry" });

            migrationBuilder.InsertData(
                table: "Friends",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { 3, 45, "Sarry" });

            migrationBuilder.InsertData(
                table: "Friends",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { 4, 65, "Tarry" });

            migrationBuilder.InsertData(
                table: "Friends",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { 5, 34, "Linda" });

            migrationBuilder.InsertData(
                table: "Friends",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { 6, 42, "Lisa" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Friends");
        }
    }
}
