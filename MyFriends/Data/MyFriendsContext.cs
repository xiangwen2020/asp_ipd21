using MyFriends.Models;
using Microsoft.EntityFrameworkCore;
using MyFriends.Data.Configurations;

namespace MyFriends.Data
{
   public  class MyFriendsContext : DbContext
    {
        public DbSet<Friend> Friends { get; set; }//Friends是table名字
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=MyFriends.db");//file 名字
        }

         protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new FriendConfiguration()).Seed();
    }
    }
}